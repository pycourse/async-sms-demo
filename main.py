import asyncio

import aioredis
from aiohttp import web, ClientSession
from task_queue import ensure_sms_delivery_endlessly
from views import send_sms, check_delivery_status, ws_check_delivery_status
from config import REDIS_URL


async def dispose_client_session(app):
    await app['session'].close()
    app['cache'].close()
    await app['cache'].wait_closed()


async def run_task_queue(app):
    asyncio.create_task(
        ensure_sms_delivery_endlessly(app['session'], app['cache'])
    )


async def create_app():
    app = web.Application()
    app.router.add_post('/send', send_sms)
    app.router.add_post('/status', check_delivery_status)
    app.router.add_get('/ws', ws_check_delivery_status)
    app['session'] = ClientSession()
    app['cache'] = await aioredis.create_redis(REDIS_URL, encoding='UTF-8')
    app.on_startup.append(run_task_queue)
    app.on_cleanup.append(dispose_client_session)
    return app


if __name__ == '__main__':
    web.run_app(create_app())
