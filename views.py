from aiohttp import web, WSMsgType
from api import call_api
from task_queue import save_task_queue, fetch_tasks_status


async def send_sms(request):
    data = await request.json()
    # FIXME add JSON schema validation for input data
    response = await call_api(request.app['session'], 'send', data)
    if not isinstance(response, str) and 'error' not in response:
        await save_task_queue(request.app['cache'], response, data['mes'])
    # FIXME specify corect HTTP status depending on sending fail or success
    return web.json_response(response)


async def check_delivery_status(request):
    data = await request.json()
    # FIXME add JSON schema validation for input data
    # FIXME should return delivery status for all tasks. Get datа from Redis, use fetch_tasks_status
    response = await call_api(request.app['session'], 'status', data)
    return web.json_response(response)


async def ws_check_delivery_status(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    async for msg in ws:
        if msg.type != WSMsgType.TEXT:
            continue

        if msg.data == 'close':
            await ws.close()
        elif msg.data == 'update':
            results = await fetch_tasks_status(request.app['cache'])
            await ws.send_json(results)
        
        # TODO should send update to ws on every tasks update. Independently on client side 'update' requests

    return ws
