import asyncio

from config import DELIVERY_STATUS_CODES, REQUEST_LIMIT_EXCEEDED_CODE, \
    REQUEST_DELAY, MAX_RETRIES
from api import call_api


def _encode_task_key(sms_id, phone):
    return f'task_{sms_id}_{phone}'


def _encode_sms_text_key(sms_id):
    return f'text_{sms_id}'


async def fetch_tasks_status(cache):
    task_result_ids = await cache.keys('result_*')
    tasks_status = []
    for task_result_id in task_result_ids:
        results = await cache.hgetall(task_result_id)
        _, sms_id = task_result_id.split('_')
        sms_text = await cache.get(_encode_sms_text_key(sms_id))
        tasks_status.append({'text': sms_text, 'results': results})
    return tasks_status


async def load_task_queue(cache):
    tasks_ids = await cache.keys('task_*')
    tasks = []
    for task in tasks_ids:
        _, sms_id, phone = task.split('_')
        tasks.append((phone, sms_id))
    return tasks


async def save_task_queue(cache, response, message_text):
    tasks = [(sms_info['phone'], str(response['id'])) for sms_info in response['phones']]
    for phone, sms_id in tasks:
        await cache.set(_encode_task_key(sms_id, phone), 0)
    await cache.set(_encode_sms_text_key(sms_id), message_text)


async def finalize_task(cache, sms_id, phone, status):
    await cache.delete(_encode_task_key(sms_id, phone))
    await update_task_status(cache, sms_id, phone, status)


async def update_task_status(cache, sms_id, phone, status):
    await cache.hset(f'result_{sms_id}', f'{phone}', status)


async def reschedule_failed_task(cache, session, sms_id, phone):
    num_of_retries = await cache.get(_encode_task_key(sms_id, phone))

    if num_of_retries + 1 <= MAX_RETRIES:
        await cache.incr(_encode_task_key(sms_id, phone))
        sms_text = await cache.get(_encode_sms_text_key(sms_id))
        response = await call_api(session, 'send', payload={'phones': phone, 'mes': sms_text})

        if not isinstance(response, str) and 'error' not in response:
            await save_task_queue(cache, response)
        # FIXME заменить на логгирование
            print(f'Rescheduled!')
        else:
        # FIXME заменить на логгирование
            print(f'Error occured while rescheduling SMS to {phone}')
            await finalize_task(cache, sms_id, phone, response.get('error_code'))
    else:
        # FIXME заменить на логгирование
        print(f'Cannot reschedule SMS to {phone}: too many retries')
        await finalize_task(cache, sms_id, phone, -999)


async def ensure_sms_delivery_endlessly(session, cache):
    while True:
        tasks = await load_task_queue(cache)

        if tasks:
            await update_sms_delivery_tasks(session, cache, tasks)

        await asyncio.sleep(REQUEST_DELAY)


async def update_sms_delivery_tasks(session, cache, tasks):
    phones, sms_ids = zip(*tasks)
    payload = {'phone': ','.join([*phones, '']), 'id': ','.join([*sms_ids, ','])}
    response = await call_api(session, 'status', payload)

    if isinstance(response, dict) and response.get('error_code') == REQUEST_LIMIT_EXCEEDED_CODE:
        return False

    for send_operation_info in response:
        recipient_phone = send_operation_info['phone']
        delivery_status = send_operation_info['status']
        sms_id = send_operation_info['id']

        await update_task_status(cache, sms_id, recipient_phone, delivery_status)

        if delivery_status in DELIVERY_STATUS_CODES['PENDING']:
            continue
        elif delivery_status in DELIVERY_STATUS_CODES['FAILED']:
            # FIXME заменить на логгирование
            print(f'SMS to {recipient_phone} failed with error code {delivery_status}, rescheduling...')
            await reschedule_failed_task(cache, session, sms_id, recipient_phone)
            continue

        await finalize_task(cache, sms_id, recipient_phone, delivery_status)