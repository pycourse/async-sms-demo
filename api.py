import os

from config import BASE_API_URL


async def call_api(session, method, payload):
    url = BASE_API_URL.format(method)

    full_payload = {
        **payload,
        'charset': 'utf-8',
        'fmt': 3,
        'op': 1,
        'login': 'devman',
        'psw': os.getenv('SMSC_PASSWORD'),
    }

    async with session.post(url, params=full_payload) as resp:
        if resp.status != 200:
            # FIXME should be exception raised ?
            return

        if resp.content_type == 'application/json':
            return await resp.json()
        return await resp.text()
